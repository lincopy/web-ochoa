<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if(!isset($_POST["nombre"])){
		header( 'HTTP/1.1 400 BAD REQUEST' );
		echo "sin nombre";
		die();
	}
	if(!isset($_POST["email"])){
		header( 'HTTP/1.1 400 BAD REQUEST' );
		echo "email inválido";
		die();	
	}
	if(!isset($_POST["mensaje"])){
		header( 'HTTP/1.1 400 BAD REQUEST' );
		echo "sin mensaje";
		die();
	}
	if(!isset($_POST["captcha"])){
		header( 'HTTP/1.1 400 BAD REQUEST' );
		echo "captcha inválido";
		die();
	}
	$captchaValue=$_POST["captcha"];
	$secret="6Le-3A8UAAAAAFQy3LPSbdoyj8jq9QDtWQQ-uTpX";
	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captchaValue);
	$responseData = json_decode($verifyResponse);

	if(!$responseData->success){
		header( 'HTTP/1.1 400 BAD REQUEST' );
		echo "captcha inválido";
		die();
	}
	$nombre=$_POST["nombre"];
	$email=$_POST["email"];
	$telefono=$_POST["telefono"];
	$mensaje=$_POST["mensaje"];
	$mensajeMail = "Nuevo contacto del sr/a ".$nombre." con email: ".$email." tel: ".$telefono." mensaje: ".$mensaje;
	mail("willian@linco.com.py", "contacto desde web",$mensajeMail);
	echo "Gracias, su mensaje fue enviado, en breve un representante se pondra en contacto.";
	die();

}else{
	header( 'HTTP/1.1 400 BAD REQUEST' );
	die();
}