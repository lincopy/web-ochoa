            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>IMPACTO SOCIAL</h1>
                            <?php else:?>
                                <h1>SOCIAL IMPACT</h1>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <a href="socialimpact3.php" class="generic-link-span-active">PARROQUIA SAN AGUSTIN <span>></span></a>
                        <?php else:?>
                            <a href="socialimpact3.php" class="generic-link-span-active">PARISH SAN AGUSTIN <span>></span></a>
                        <?php endif;?>


                        <div class="wrapper-social-paragraph">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="generic-title-18px">La parroquia San Agustín fue renovada con aportes de la comunidad y empresa constructora.</h1>
                                <p class="generic-title-italic">Los fieles de la parroquia San Agustín, liderados por Pa´í Rafaelito, movilizaron recursos para agrandar el templo, con aportes que incluyeron materiales de construcción procedentes de la obra del túnel Semidei.</p>
                                <p class="generic-paragraph">La parroquia San Agustín, ubicada en el barrio Loma Pytá de Asunción, se caracteriza por contar con fieles muy activos que trabajan codo a codo en beneficio de la iglesia y la comunidad. El Padre Rafael Tanasio, más conocido como Pa´í Rafaelito, es muy querido y respetado por los fieles, quienes asisten con regularidad a las misas diarias que él ofrece.</p>
                                <p class="generic-paragraph">Pa´í Rafaelito recuerda que años atrás la parroquia era mucho más pequeña y necesitaba mejoras importantes en su infraestructura. “Había mucha gente en la misa de los domingos y entre semana y el templo nos quedaba muy chico”, recuerda. Fue entonces cuando se reunió con un grupo de fieles y decidieron tomar el desafío de agrandar y mejorar la parroquia, a través de diversas actividades que incluyeron rifas y ventas de comida.</p>
                                <p class="generic-paragraph">Al mismo tiempo se hallaba en construcción el túnel Semidei, que se encuentra a metros de la parroquia, y tanto obreros como ingenieros asistían a las misas semanales nocturnas, luego de la jornada laboral, siendo testigos de la aglomeración de gente que había por la falta de espacio. Luego de conversar con los encargados de la obra, quienes ya conocían la situación de la iglesia de boca de los propios obreros, un domingo después de misa, se acercaron a la parroquia a informar que aportarían materiales de construcción como un apoyo al proyecto. Bolsas de cemento y cal, hierro, piedra triturada y arena llegaron a la parroquia y alivianaron en gran manera los costos del proyecto, permitiendo que se realizaran los trabajos con mayor rapidez.</p>
                                <p class="generic-paragraph">Hoy día la parroquia cuenta con espacio para recibir cómodamente a más fieles, además de mostrarse renovada y embellecida. “Con eso logramos terminar nuestro templo, que ahora sí es grande y hermoso”, añade resaltando también la importancia de que las empresas sean solidarias con las comunidades donde realizan trabajos. “Se nota la solidaridad de la empresa, cuando hay progreso no hay que olvidar lo humano” y finaliza “es muy importante humanizar la economía, como dijo el Papa Francisco cuando nos visitó el año pasado, hay que progresar sin olvidar a las personas”.</p>

                            <?php else:?>
                                <h1 class="generic-title-18px">The parish of San Agustín was renewed with contributions from the community and construction company.</h1>
                                <p class="generic-title-italic">The faithful of the parish of San Agustín, led by Pa'í Rafaelito, mobilized resources to enlarge the temple, with contributions that included construction materials from the work of the Semidei tunnel.</p>
                                <p class="generic-paragraph">The San Agustín parish, located in the Loma Pytá neighborhood of Asuncion, is characterized by very active faithful working side by side for the benefit of the church and the community. Father Rafael Tanasio, better known as Pa'í Rafaelito, is well liked and respected by the faithful, who attend regularly to the daily masses he offers.</p>
                                <p class="generic-paragraph">Raphael priest remembers that years ago the parish was much smaller and needed major improvements in its infrastructure. "There were a lot of people at Sunday mass and during the week and the temple we were very young," he recalls. It was then that he met with a group of faithful and decided to take the challenge of enlarging and improving the parish, through various activities that included raffles and food sales.</p>
                                <p class="generic-paragraph">At the same time, the Semidei tunnel, which is located a few meters from the parish, was under construction, and both workers and engineers attended the masses at night, after the working day, witnessing the agglomeration of people who were missing of space . After conversing with those in charge of the work, who already knew the situation of the church by the workers themselves, to Sunday after Mass, they approached the parish to inform that they would provide building materials as a support for the project. Cement and lime, iron, crushed stone and sand came to the parish and greatly alleviated the costs of the project, allowing the work to be carried out more quickly.</p>
                                <p class="generic-paragraph">Today the parish has space to comfortably accommodate more faithful, as well as being refreshed and embellished. "With that we managed to finish our temple, which is now big and beautiful," he adds, also stressing the importance of companies being in solidarity with the communities where they work. "It shows the solidarity of the company, when there is progress we must not forget the human" and ends "it is very important to humanize the economy, as Pope Benedict said when we visited last year, we must progress without forgetting people" .</p>

                            <?php endif;?>


                        </div>

                        <div class="video">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/YsnsXZzn23s" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div id="wrapper-social-web">
                            <?php if($_SESSION['lang']=='es'):?>

                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>

                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>

                        </div>
                    </div>

                    <div class="col-md-4 social-wrapper-main">
                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/agustin/photo1.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/agustin/photo4.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/agustin/photo2.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work">
                            <img src="../images/social/agustin/photo3.png" class="img-responsive"/>
                        </div>

                        <div id="wrapper-social-mobile">
                            <?php if($_SESSION['lang']=='es'):?>

                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>

                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
