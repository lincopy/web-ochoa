            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>NUESTRAS OBRAS</h1>
                            <?php else:?>
                                <h1>OUR WORKS</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                 
                            <img src="../images/works/work1.jpg" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
<<<<<<< HEAD
                        <h1 class="generic-title-18px">Construcción de Obras Viales en la margen derecha Paquete B (Costanera de Carmen del Paraná).</h1>

                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Construcción de Obras Viales en la margen derecha Paquete B (Costanera de Carmen del Paraná).</p>
=======
                        <?php if($_SESSION['lang']=='es'):?>
                            <h1 class="generic-title-18px">Nombre de la Obra:</h1>
                            <p class="generic-paragraph">Construcción de Obras Viales en la margen derecha Paquete B (Costanera de Carmen del Paraná).</p>

                            <h1 class="generic-title-18px">Inicio:</h1>
                            <p class="generic-paragraph">October 2007</p>

                            <h1 class="generic-title-18px">Fin:</h1>
                            <p class="generic-paragraph">January 2011</p>

                            <h1 class="generic-title-18px">Breve Descripción:</h1>
                            <p class="generic-paragraph">Se realizaron las excavaciones necesarias para preparar el terraplén que sirvió de base a la carpeta de concreto asfáltico de la circunvalación de la ciudad de Carmen del Paraná. Además de las obras complementarias realizadas (alcantarillas).</p>
                            <p class="generic-paragraph">Los más de 4.700 pobladores de la ciudad se vieron beneficiados con este proyecto que permite un rápido acceso y creo un nuevo atractivo turístico al departamento de Itapúa.</p>

                            <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                            <p class="generic-paragraph">USD 45.285.049,11</p>

                            <h1 class="generic-title-18px">Contratante:</h1>
                            <p class="generic-paragraph">Entidad Binacional Yacyreta</p>

                            <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                        <?php else:?>
                            <h1 class="generic-title-18px">Name of the work:</h1>
                            <p class="generic-paragraph">Construction of Road Works on the right bank Package B (Costanera de Carmen del Paraná).</p>

                            <h1 class="generic-title-18px">Start:</h1>
                            <p class="generic-paragraph">Octubre 2007</p>
>>>>>>> 4b5dbb411db2c95ca9224ed96742d41b947936dc

                            <h1 class="generic-title-18px">End:</h1>
                            <p class="generic-paragraph">Enero 2011</p>

                            <h1 class="generic-title-18px">Short description:</h1>
                            <p class="generic-paragraph">Excavations were carried out to prepare the embankment that served as the base for the concrete asphalt of the circumvallation of the city of Carmen del Paraná. In addition to the complementary works carried out (sewers).</p>
                            <p class="generic-paragraph">The more than 4,700 inhabitants of the city benefited from this project that allows quick access and created a new tourist attraction to the department of Itapúa.</p>

                            <h1 class="generic-title-18px">Amount of the Contract in USD:</h1>
                            <p class="generic-paragraph">USD 45.285.049,11</p>

                            <h1 class="generic-title-18px">Contractor:</h1>
                            <p class="generic-paragraph">Binational Entity Yacyreta</p>

                            <a class="generic-link-underline" href="ourworks.php">Go back</a>
                        <?php endif;?>

                    </div>
                    <div class="col-md-2"></div>
                </div>
                   
            </div>

            <?php include("footer.php"); ?>
