<?php if(!isset($_SESSION)){session_start();}?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Ocho A S.A.</title>
        <meta charset="utf-8" />
        <script src="../js/jquery-3.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="../css/8a.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                function fullscreen() {
                    jQuery('#home-header').css({
                        width: jQuery(window).width(),
                        height: jQuery(window).height()
                    });
                }

                fullscreen();

                jQuery(window).resize(function() {
                    fullscreen();
                });

                $('ul li a').click(function() {
                    $('li a').removeClass("active");
                    $(this).addClass("active");
                });
                
                $('#button-down').click(function() {
                    var link = $(this);
                    var anchor = link.attr('href');

                    $('html, body').stop().animate({
                        scrollTop: jQuery(anchor).offset().top
                    }, 1000);

                    return false;
                });
            });
        </script>
    </head>

    <body>
        <div id="wrapper">
