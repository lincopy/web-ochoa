            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>NUESTRAS OBRAS</h1>
                            <?php else:?>
                                <h1>OUR WORKS</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                                 <img src="../images/works/work2.jpg" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
<<<<<<< HEAD
                        <h1 class="generic-title-18px">Autopista Ñu Guazú. Tramo 1</h1>


                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Ejecución de la obra Construcción de la Autopista Ñu Guazú: Asunción – Luque (6,3 Km) Tramo 1</p>
=======
                        <?php if($_SESSION['lang']=='es'):?>
                            <h1 class="generic-title-18px">Nombre de la Obra:</h1>
                            <p class="generic-paragraph">Autopista Ñu Guazú. Tramo 1</p>

                            <h1 class="generic-title-18px">Inicio:</h1>
                            <p class="generic-paragraph">Octubre 2012</p>

                            <h1 class="generic-title-18px">Fin:</h1>
                            <p class="generic-paragraph">Marzo 2014</p>

                            <h1 class="generic-title-18px">Breve Descripción:</h1>
                            <p class="generic-paragraph">Los trabajos del Tramo 1 de la Autopista Ñu Guazú consistieron en la ejecución de 6,3Km de sub base granular estabilizada, base granular, carpeta de concreto asfáltico.</p>
                            <p class="generic-paragraph">Además de puentes, pasos a desnivel, alcantarillas y la iluminación correspondiente, todo esto permite el rápido acceso a la ciudad de Asunción desde Luque, beneficiando a su vez a pobladores de ciudades aledañas.</p>

                            <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                            <p class="generic-paragraph">USD 29.485.124,09</p>

                            <h1 class="generic-title-18px">Contratante:</h1>
                            <p class="generic-paragraph">MOPC</p>

                            <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                        <?php else:?>
                            <h1 class="generic-title-18px">Name of the work:</h1>
                            <p class="generic-paragraph">Autopista Ñu Guazú. Tramo 1</p>

                            <h1 class="generic-title-18px">Start:</h1>
                            <p class="generic-paragraph">October 2012</p>
>>>>>>> 4b5dbb411db2c95ca9224ed96742d41b947936dc

                            <h1 class="generic-title-18px">End:</h1>
                            <p class="generic-paragraph">March 2014</p>

                            <h1 class="generic-title-18px">Short description:</h1>
                            <p class="generic-paragraph">The works of Section 1 of the Ñu Guazú Highway consisted of the execution of 6.3 Km of stabilized granular subbase, granular base, asphalt concrete folder.</p>
                            <p class="generic-paragraph">In addition to bridges, overpasses, sewers and corresponding lighting, all this allows quick access to the city of Asunción from Luque, benefiting in turn people from surrounding cities.</p>

                            <h1 class="generic-title-18px">Amount of the Contract in USD:</h1>
                            <p class="generic-paragraph">USD 29.485.124,09</p>

                            <h1 class="generic-title-18px">Contractor:</h1>
                            <p class="generic-paragraph">MOPC</p>

                            <a class="generic-link-underline" href="ourworks.php">Go back</a>
                        <?php endif;?>

                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <?php include("footer.php"); ?>
