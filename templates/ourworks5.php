            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <h1>NUESTRAS OBRAS</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                            <img src="../images/works/work5.jpg" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
                        <h1 class="generic-title-18px">Rehabilitación y Pavimentación del tramo Vaquería - empalme Ruta X</h1>

                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Rehabilitación y Pavimentación del tramo Vaquería - empalme Ruta 10</p>

                        <h1 class="generic-title-18px">Inicio:</h1>
                        <p class="generic-paragraph">Septiembre 2015</p>

                        <h1 class="generic-title-18px">Fin:</h1>
                        <p class="generic-paragraph">Noviembre 2017</p>

                        <h1 class="generic-title-18px">Breve Descripción:</h1>
                        <p class="generic-paragraph">Los trabajos consisten en la rehabilitación y pavimentación del tramo que une la ciudad de Vaquería con el empalme a la Ruta 10, con una longitud total de 57,31 km. ubicado entre las progresivas 64+230 y 121+545.</p>
                        <p class="generic-paragraph">Terraplén, sub-base, carpeta asfáltica, puentes.</p>

                        <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                        <p class="generic-paragraph">USD 46.050.353,67</p>

                        <h1 class="generic-title-18px">Contratante:</h1>
                        <p class="generic-paragraph">MOPC</p>
                        <div class="video">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/9VgFhDdKHk8" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <?php include("footer.php"); ?>
