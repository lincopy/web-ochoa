            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>IMPACTO SOCIAL</h1>
                            <?php else:?>
                                <h1>SOCIAL IMPACT</h1>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <a href="socialimpact4.php" class="generic-link-span-active">POBLADORES TRANSCHACO <span>></span></a>
                        <?php else:?>
                            <a href="socialimpact4.php" class="generic-link-span-active">POPULATORS TRANSCHACO <span>></span></a>
                        <?php endif;?>


                        <div class="wrapper-social-paragraph">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="generic-title-18px">Pobladores valoran nuevas obras en la Transchaco</h1>
                                <p class="generic-title-italic">Tanto comerciantes como pobladores de la zona Semidei de la ruta Transchaco comentan las mejoras en el tránsito y el paisaje mismo de la ruta, a partir de la apertura del túnel Semidei, el primero en su tipo del país.</p>
                                <p class="generic-paragraph">La ruta Transchaco, que inicia en el Jardín Botánico y cruza todo el Chaco paraguayo, es una vía rápida de ingreso y salida de Asunción que comunica la ciudad con Mariano Roque Alonso y otras ciudades aledañas. Con el paso de los años, el crecimiento vertiginoso de estas ciudades aumentó exponencialmente el número de vehículos que transitan por la ruta, convirtiéndose en un verdadero desafío el tránsito diario, sobre todo en horas pico.</p>
                                <p class="generic-paragraph">El túnel Semidei, el primero en su tipo del país, se inauguró en marzo de 2014 para proporcionar un cruce rápido que agilizara el paso de vehículos entre la Ruta Transchaco y la Avenida Semidei. “Era todo un problema manejar en estos lugares en aquella época”, recuerda Ernando Espínola, poblador de la zona, quien considera que este tipo de obras viales mejora mucho, no solo el tránsito, sino el comercio y la comunidad misma.</p>
                                <p class="generic-paragraph">Petronela de Báez, comerciante de la zona, recuerda que la Transchaco se veía como un camino viejo y que las mejoras en la ruta, entre las que se encuentra el túnel, mejoraron mucho el aspecto de la zona. “Por fin en Paraguay también un túnel”, comenta. Añade que aunque el túnel no resuelve por completo el problema de la congestión vehicular, aliviana el tránsito, y espera que continúen las obras planificadas por el gobierno para contar con un sistema de rutas y accesos viales óptimos.</p>
                                <p class="generic-paragraph">Para Agustín Alvarenga, comerciante, la mejora se da de manera paulatina e involucra a los propios ciudadanos. “Tenemos que aprender a utilizar las vías, a respetar las señales de tránsito”, asegura. Añade que los beneficios no son solo para los pobladores actuales sino también para sus hijos. “Como padre, ojalá que podamos seguir así creciendo para el futuro de nuestros hijos”, resalta y concluye que cuando un pueblo crece, con este tipo de obras y mejoras, también lo hacen la familia y la comunidad.</p>

                            <?php else:?>
                                <h1 class="generic-title-18px">Pobladores value new works in Transchaco</h1>
                                <p class="generic-title-italic">Both traders and settlers from the Semidei area of ​​the Transchaco route comment on the improvements in traffic and the landscape of the route, starting with the opening of the Semidei tunnel, the first of its kind in the country.</p>
                                <p class="generic-paragraph">The Transchaco route, which starts at the Botanic Garden and crosses the whole of the Paraguayan Chaco, is a quick way in and out of Asunción that connects the city with Mariano Roque Alonso and other surrounding cities. Over the years, the rapid growth of these cities has exponentially increased the number of vehicles that transit the route, making everyday traffic a daily challenge, especially at peak times.</p>
                                <p class="generic-paragraph">The Semidei tunnel, the first of its kind in the country, was inaugurated in March 2014 to provide a fast intersection that would expedite the passage of vehicles between the Transchaco Route and Semidei Avenue. "It was quite a problem to drive in these places at the time," recalls Ernando Espínola, a resident of the area, who believes that this type of road works improves a lot, not only traffic, but commerce and the community itself.</p>
                                <p class="generic-paragraph">Petronela de Baez, a merchant in the area, remembers that the Transchaco looked like an old road and that improvements in the route, including the tunnel, greatly improved the appearance of the area. "Finally in Paraguay also a tunnel," he says. He adds that although the tunnel does not completely solve the problem of vehicular congestion, it alleviates traffic, and expects that the works planned by the government will continue to have a system of routes and optimal access roads.</p>
                                <p class="generic-paragraph">For Agustín Alvarenga, merchant, the improvement is given gradually and involves the citizens themselves. "We have to learn to use the roads, to respect the traffic signals," he says. He adds that the benefits are not only for the present settlers but also for their children. "As a father, hopefully we can continue to grow for the future of our children," he emphasizes and concludes that when a people grows, with these kinds of works and improvements, so do the family and the community.</p>

                            <?php endif;?>

                        </div>

                        <div class="video">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/PcX8ZvDgAkA" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div id="wrapper-social-web">
                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                            <?php endif;?>

                        </div>
                    </div>

                    <div class="col-md-4 social-wrapper-main">
                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/transchaco/photo1.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/transchaco/photo4.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/transchaco/photo2.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work">
                            <img src="../images/social/transchaco/photo3.png" class="img-responsive"/>
                        </div>

                        <div id="wrapper-social-mobile">
                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
