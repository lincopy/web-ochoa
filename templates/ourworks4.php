            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>NUESTRAS OBRAS</h1>
                            <?php else:?>
                                <h1>OUR WORKS</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                            <img src="../images/works/work4.png" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
<<<<<<< HEAD
                        <h1 class="generic-title-18px">Construcción de pasos a desniveles, intersección Avda. Madame Linch y Ruta Transchaco.</h1>

                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Construcción de pasos a desniveles en la ciudad de Asunción, intersección Avda. Madame Linch y Ruta Transchaco.</p>
=======
                        <?php if($_SESSION['lang']=='es'):?>
                            <h1 class="generic-title-18px">Nombre de la Obra:</h1>
                            <p class="generic-paragraph">Construcción de pasos a desniveles, intersección Avda. Madame Linch y Ruta Transchaco.</p>

                            <h1 class="generic-title-18px">Inicio:</h1>
                            <p class="generic-paragraph">Octubre 2012</p>

                            <h1 class="generic-title-18px">Fin:</h1>
                            <p class="generic-paragraph">Abril 2014</p>

                            <h1 class="generic-title-18px">Breve Descripción:</h1>
                            <p class="generic-paragraph">El primer paso a desnivel del país, fue construido de hormigón estructural, alcantarillas y carpeta de concreto asfáltico.</p>
                            <p class="generic-paragraph">Se construyó con el objetivo de descongestionar y agilizar el tráfico vehicular en el acceso de la ciudad de Mariano Roque Alonso.</p>

                            <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                            <p class="generic-paragraph">USD 6.287.749,18</p>

                            <h1 class="generic-title-18px">Contratante:</h1>
                            <p class="generic-paragraph">MOPC</p>

                            <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                        <?php else:?>
                            <h1 class="generic-title-18px">Name of the work:</h1>
                            <p class="generic-paragraph">Construction of steps to unevenness, intersection Avda. Madame Linch and Ruta Transchaco.</p>

                            <h1 class="generic-title-18px">Start:</h1>
                            <p class="generic-paragraph">October 2012</p>
>>>>>>> 4b5dbb411db2c95ca9224ed96742d41b947936dc

                            <h1 class="generic-title-18px">End:</h1>
                            <p class="generic-paragraph">April 2014</p>

                            <h1 class="generic-title-18px">Short description:</h1>
                            <p class="generic-paragraph">The first overpass of the country, was constructed of structural concrete, sewers and asphalt concrete folder.</p>
                            <p class="generic-paragraph">It was built with the aim of decongesting and speeding up vehicular traffic in the access of the city of Mariano Roque Alonso.</p>

                            <h1 class="generic-title-18px">Amount of the Contract in USD:</h1>
                            <p class="generic-paragraph">USD 6.287.749,18</p>

                            <h1 class="generic-title-18px">Contractor:</h1>
                            <p class="generic-paragraph">MOPC</p>

                            <a class="generic-link-underline" href="ourworks.php">Go back</a>
                        <?php endif;?>

                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <?php include("footer.php"); ?>
