            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>NUESTRAS OBRAS</h1>
                            <?php else:?>
                                <h1>OUR WORKS</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-4 aboutus-content-margin">
                        <div class="ourworks-wrapper-main">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work1.jpg" class="img-responsive" width="360" height="211"/>
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <h1>Construcción de Obras Viales en la margen derecha Paquete B (Costanera de Carmen del Paraná)</h1>
                                    <?php else:?>
                                        <h1>Construction of Road Works on the right bank Package B (Costanera de Carmen del Paraná)</h1>
                                    <?php endif;?>

                                </div>
                                <?php if($_SESSION['lang']=='es'):?>
                                    <a class="generic-link" href="ourworks1.php">Más Detalles</a>
                                <?php else:?>
                                    <a class="generic-link" href="ourworks1.php">More details</a>
                                <?php endif;?>

                            </div>
                        </div>

                        <div class="ourworks-wrapper-main generic-margin-top-65px">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work2.jpg" class="img-responsive" width="360" height="211" />
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <h1>Autopista Ñu Guazú. Tramo 1</h1>
                                    <?php else:?>
                                        <h1>Freeway Ñu Guazú. Tramo 1</h1>
                                    <?php endif;?>

                                </div>

                                <?php if($_SESSION['lang']=='es'):?>
                                    <a class="generic-link" href="ourworks2.php">Más Detalles</a>
                                <?php else:?>
                                    <a class="generic-link" href="ourworks2.php">More details</a>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 aboutus-content-margin">
                        <div class="ourworks-wrapper-main">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work3.png" class="img-responsive">
                                <!--<img src="../images/works/work3.jpg" class="img-responsive" width="360" height="211"/>-->
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <h1>Rehabilitación y Pavimentación del Tramo Caaguazú – Yhu – Vaquería y Accesos.</h1>
                                    <?php else:?>
                                        <h1>Rehabilitation and Paving of the Section Caaguazú – Yhu – Vaquería And Access.</h1>
                                    <?php endif;?>

                                </div>

                                <?php if($_SESSION['lang']=='es'):?>
                                    <a class="generic-link" href="ourworks3.php">Más Detalles</a>
                                <?php else:?>
                                    <a class="generic-link" href="ourworks3.php">More details</a>
                                <?php endif;?>
                            </div>
                        </div>

                        <div class="ourworks-wrapper-main generic-margin-top-65px">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work4.png" class="img-responsive" width="360" height="211"/>
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <h1>Construcción de pasos a desniveles, intersección Avda. Madame Linch y Ruta Transchaco.</h1>
                                    <?php else:?>
                                        <h1>Construction of steps at unevenness, intersection Avda. Madame Linch y Ruta Transchaco.</h1>
                                    <?php endif;?>

                                </div>

                                <?php if($_SESSION['lang']=='es'):?>
                                    <a class="generic-link" href="ourworks4.php">Más Detalles</a>
                                <?php else:?>
                                    <a class="generic-link" href="ourworks4.php">More details</a>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div id="ourworks-slogan" class="aboutus-content-margin">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="aboutus-slogan-title">MEJORAMIENTO DE CAMINOS OBRAS VIALES Y CIVILES ESTACIONES ELÉCTRICAS SUPERCARRETERAS ALCANTARILLADO EMPEDRADOS CARRETERAS PAVIMENTOS ENRIPIADOS REPRESAS PUENTES</h1>
                            <?php else:?>
                                <h1 class="aboutus-slogan-title">IMPROVEMENT OF ROADS VIAL AND CIVIL WORKS ELECTRICAL STATIONS SUPERCARRETERAS SEWERING STACKED ROADS ENRICHED PAVEMENTS DAMAGES BRIDGES</h1>
                            <?php endif;?>

                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-4 aboutus-content-margin">
                        <div class="ourworks-wrapper-main">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work5.jpg" class="img-responsive" width="360" height="211"/>
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <h1>Rehabilitación y Pavimentación del tramo Vaquería - empalme Ruta X</h1>
                                </div>

                                <a class="generic-link" href="ourworks5.php">Más Detalles</a>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 aboutus-content-margin">
                        <div class="ourworks-wrapper-main">
                            <div class="ourworks-wrapper-work">
                                <img src="../images/works/work3.jpg" class="img-responsive">
                                <!--<img src="../images/works/work3.jpg" class="img-responsive" width="360" height="211"/>-->
                            </div>

                            <div class="aboutus-content-other">
                                <div class="wrapper-ourworks-content">
                                    <h1>Rehabilitación de la Ruta nro. 9 Presidente Carlos Antonio Lopez</h1>
                                </div>

                                <a class="generic-link" href="ourworks6.php">Más Detalles</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <?php include("footer.php"); ?>
