            <?php if(!isset($_SESSION)){session_start();}   ?>
            <div class="background-footer"></div>
            <div id="home-footer" class="container">
                <div class="row">
                    <div class="col-md-3 generic-padding-0px">
                        <div class="wrapper-footer-logo">
                            <div class="footer-logo"></div>
                            <h1 class="footer-logo-title">© 2017 Ocho A S.A.</h1>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 wrapper-footer-contact">
                        <div class="contact-border contact-margin-right contact-margin-bottom">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="contact-title">Oficina central:</h1>
                            <?php else:?>
                                <h1 class="contact-title">Central office:</h1>
                            <?php endif;?>

                            <p class="contact-paragraph contact-paragraph-margin">Av. Artigas 1921 - 4to Piso</p>
                            <p class="contact-paragraph contact-paragraph-margin">Asunción, Paraguay</p>
                            <p class="contact-paragraph">Telefax: +595 21 299585</p>
                        </div>

                        <div class="contact-border">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="contact-title">Operaciones y suministros:</h1>
                            <?php else:?>
                                <h1 class="contact-title">Operations and supplies:</h1>
                            <?php endif;?>

                            <p class="contact-paragraph contact-paragraph-margin">Ruta 2 Km 18,5 esq. Santa Cruz Adorno</p>
                            <p class="contact-paragraph contact-paragraph-margin">Capiatá, Paraguay</p>
                            <p class="contact-paragraph">Telefax: +595 228 630125</p>
                        </div>
                    </div>
                    
                    <div class="col-md-3 generic-padding-0px">
                        <div class="wrapper-footer-logo">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="footer-linco-title">desarrollado por</h1>
                            <?php else:?>
                                <h1 class="footer-linco-title">desing by</h1>
                            <?php endif;?>

                            <a class="footer-linco" href="https://www.linco.com.py/" target="_blank"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
