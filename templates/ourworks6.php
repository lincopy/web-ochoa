            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <h1>NUESTRAS OBRAS</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                            <img src="../images/works/work3.jpg" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
                        <h1 class="generic-title-18px">Rehabilitación de la Ruta nro. 9 Presidente Carlos Antonio Lopez</h1>

                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Rehabilitación de la Ruta nro. 9 Presidente Carlos Antonio Lopez. Tramo Mariscal Estigarribia - Infante Rivarola</p>

                        <h1 class="generic-title-18px">Inicio:</h1>
                        <p class="generic-paragraph">Febrero 2014</p>

                        <h1 class="generic-title-18px">Fin:</h1>
                        <p class="generic-paragraph">Septiembre  2015</p>

                        <h1 class="generic-title-18px">Breve Descripción:</h1>
                        <p class="generic-paragraph">Terraplén, bacheo superficial y profundo, concreto asfáltico, tratamiento superficial en banquina.</p>

                        <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                        <p class="generic-paragraph">USD 6.877.989,30</p>

                        <h1 class="generic-title-18px">Contratante:</h1>
                        <p class="generic-paragraph">MOPC</p>
                        
                        <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <?php include("footer.php"); ?>
