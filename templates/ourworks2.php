            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>NUESTRAS OBRAS</h1>
                            <?php else:?>
                                <h1>OUR WORKS</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 aboutus-content-margin">
                        <div class="ourworks-wrapper-work">
                            <img src="../images/works/work3.png" class="img-responsive"/>
                        </div>
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 generic-margin-bottom-65px">
<<<<<<< HEAD
                        <h1 class="generic-title-18px">Rehabilitación y Pavimentación del Tramo Caaguazú – Yhu – Vaquería y Accesos.</h1>

                        <h1 class="generic-title-18px">Nombre del Contrato:</h1>
                        <p class="generic-paragraph">Rehabilitación y Pavimentación del Tramo Caaguazú – Yhu – Vaquería y Accesos.</p>
=======
                        <?php if($_SESSION['lang']=='es'):?>
                            <h1 class="generic-title-18px">Nombre de la Obra:</h1>
                            <p class="generic-paragraph">Rehabilitación y Pavimentación del Tramo Caaguazú – Yhu – Vaquería y Accesos.</p>

                            <h1 class="generic-title-18px">Inicio:</h1>
                            <p class="generic-paragraph">Marzo 2012</p>

                            <h1 class="generic-title-18px">Fin:</h1>
                            <p class="generic-paragraph">Junio 2014</p>

                            <h1 class="generic-title-18px">Breve Descripción:</h1>
                            <p class="generic-paragraph">Se preparó el terraplén, base granular y base de suelo cemento. La carpeta de concreto asfáltico tuvo una extensión de 32km de longitud que unen las ciudades de Caaguazú y Vaquería.</p>
                            <p class="generic-paragraph">Entre las obras de arte principales se construyeron puentes de gran porte.</p>

                            <h1 class="generic-title-18px">Monto del Contrato en USD:</h1>
                            <p class="generic-paragraph">USD 22.608.564,37</p>

                            <h1 class="generic-title-18px">Contratante:</h1>
                            <p class="generic-paragraph">MOPC</p>
                        <?php else:?>
                            <h1 class="generic-title-18px">Name of the work:</h1>
                            <p class="generic-paragraph">Rehabilitation and Paving of the Section Caaguazú – Yhu – Vaquería y Access.</p>

                            <h1 class="generic-title-18px">Start:</h1>
                            <p class="generic-paragraph">March 2012</p>
>>>>>>> 4b5dbb411db2c95ca9224ed96742d41b947936dc

                            <h1 class="generic-title-18px">End:</h1>
                            <p class="generic-paragraph">June 2014</p>

                            <h1 class="generic-title-18px">Short description:</h1>
                            <p class="generic-paragraph">The embankment, granular base and cement base were prepared. The asphaltic concrete folder had a length of 32km that joins the cities of Caaguazú and Vaquería.</p>
                            <p class="generic-paragraph">Among the principal works of art were large bridges.</p>

                            <h1 class="generic-title-18px">Amount of the Contract USD:</h1>
                            <p class="generic-paragraph">USD 22.608.564,37</p>

                            <h1 class="generic-title-18px">Contractor:</h1>
                            <p class="generic-paragraph">MOPC</p>
                        <?php endif;?>


                        <a class="generic-link-underline" href="ourworks.php">Volver atrás</a>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>

            <?php include("footer.php"); ?>
