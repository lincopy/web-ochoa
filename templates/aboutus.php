            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>
            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>QUIÉNES SOMOS</h1>
                            <?php else:?>
                                <h1>ABOUT US</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <p class="generic-paragraph">Ocho A S.A. se constituye como empresa, dedicada a las construcciones de caminos, puentes y alcantarillados, el 12 de julio de 1976. Desde entonces, y a lo largo de más de 30 años de actividad en los servicios de ingeniería, la empresa ha ampliado sus actividades de construcciones, llevando a cabo obras civiles y electromecánicas, además de las excelentes obras viales por las cuales es reconocida.</p>
                            <p class="generic-paragraph">Hoy, los directores de Ocho A S.A. se enorgullecen de presentar una de las empresas más pujantes del Paraguay.</p>
                            <p class="generic-paragraph">Adaptándose a los estándares vanguardistas, Ocho A S.A. es la primera empresa en Paraguay, dentro del rubro de construcciones viales, en lograr la certificación de su Sistema de Gestión de Calidad bajo la Norma Internacional ISO 9001 en la versión 2015 para el "DISEÑO Y EJECUCIÓN DE OBRAS VIALES".</p>
                            <p class="generic-paragraph">A través de esta metodología de trabajo se han definido una Política de Calidad, Misión y Visión de Ocho A S.A.</p>
                        <?php else:?>
                            <p class="generic-paragraph">Ocho A SA was incorporated as a company, dedicated to the construction of roads, bridges and sewers, on July 12, 1976. Since then, throughout more than 30 years of activity in engineering services, the company has expanded its activities Of construction, carrying out civil and electromechanical works, in addition to the excellent road works for which it is recognized.</p>
                            <p class="generic-paragraph">Today, the directors of Ocho A S.A. Are proud to present one of the most powerful companies in Paraguay.</p>
                            <p class="generic-paragraph">Adapting to avant-garde standards, Ocho A S.A. Is implementing a Quality Management System based on the International Standard ISO 9001, which contributes to the growth of the company.</p>
                            <p class="generic-paragraph">Through this work methodology, a Quality, Mission and Vision Policy of Ocho A S.A.</p>
                        <?php endif;?>


                        <div class="aboutus-content-other">
                            <div class="wrapper-aboutus-content">
                                <?php if($_SESSION['lang']=='es'):?>
                                    <h1>POLÍTICA DE CALIDAD</h1>
                                <?php else:?>
                                    <h1>QUALITY POLITICS</h1>
                                <?php endif;?>
                            </div>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph">Ocho A S.A. orienta sus actividades hacia la mejora continua de sus procesos, para asegurar el cumplimiento de los requisitos establecidos y dar satisfacción plena a sus clientes.</p>
                            <?php else:?>
                            <p class="generic-paragraph">Ocho A S.A. Orientates its activities towards the continuous improvement of its processes, to assure the fulfillment of the established requirements and to give full satisfaction to its clients.</p>
                            <?php endif;?>

                        </div>

                        <div class="aboutus-content-other">
                            <div class="wrapper-aboutus-content">
                                <?php if($_SESSION['lang']=='es'):?>
                                    <h1>MISIÓN</h1>
                                <?php else:?>
                                    <h1>MISSION</h1>
                                <?php endif;?>
                            </div>

                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph">Somos una empresa dedicada a la construcción civil, vial y electromecánica.</p>
                                <p class="generic-paragraph">Mantenemos la calidad de nuestras obras cumpliendo con los requisitos establecidos por nuestros clientes, soportados por la experiencia, la excelencia profesional, y el desarrollo tecnológico permanente.</p>
                                <p class="generic-paragraph">Nos relacionamos con el entorno manteniendo la ética profesional, y logramos el retorno sobre la inversión a partir de un crecimiento sostenido, contribuyendo con el país.</p>
                            <?php else:?>
                                <p class="generic-paragraph">We are a company dedicated to civil construction, road and electromechanical.</p>
                                <p class="generic-paragraph">We maintain the quality of our works complying with the requirements established by our clients, supported by experience, professional excellence, and permanent technological development.</p>
                                <p class="generic-paragraph">We relate to the environment while maintaining professional ethics, and we achieve a return on investment based on sustained growth, contributing to the country.</p>
                            <?php endif;?>

                        </div>

                        <div class="aboutus-content-other">
                            <div class="wrapper-aboutus-content">
                                <?php if($_SESSION['lang']=='es'):?>
                                    <h1>VISIÓN</h1>
                                <?php else:?>
                                    <h1>VISION</h1>
                                <?php endif;?>

                            </div>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph">Ser reconocidos como empresa líder en el mercado de la construcción, entregando productos y servicios de alta calidad con absoluto compromiso con los clientes.</p>
                            <?php else:?>
                                <p class="generic-paragraph">
                                    Be recognized as a leading company in the construction market, delivering high quality products and services with absolute commitment to customers.</p>
                            <?php endif;?>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="generic-margin-bottom-65px">
                            <div class="wrapper-aboutus-content">
                                <?php if($_SESSION['lang']=='es'):?>
                                    <h1>PLANTEL GERENCIAL</h1>
                                <?php else:?>
                                    <h1>MANAGEMENT PLANT</h1>
                                <?php endif;?>

                            </div>

                            <p class="generic-paragraph">Ing. Luis Alberto Pettengill Vacca</p>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph-subtitle">Presidente</p>
                            <?php else:?>
                                <p class="generic-paragraph-subtitle">President</p>
                            <?php endif;?>


                            <p class="generic-paragraph">Ing. Luis Alberto Pettengill Castillo</p>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph-subtitle">Director Ejecutivo</p>
                            <?php else:?>
                                <p class="generic-paragraph-subtitle">Executive Director</p>
                            <?php endif;?>


                            <p class="generic-paragraph">Lic. Daniel Meza</p>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph-subtitle">Gerente de Administración y Finanzas</p>
                            <?php else:?>
                                <p class="generic-paragraph-subtitle">Administration and Finance Manager</p>
                            <?php endif;?>


                            <p class="generic-paragraph">Lic. Blanca Pettengill</p>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph-subtitle">Gerente de Recursos Humanos</p>
                            <?php else:?>
                                <p class="generic-paragraph-subtitle">Human resources manager</p>
                            <?php endif;?>


                            <p class="generic-paragraph">Ing. Benicio Núñez</p>
                            <?php if($_SESSION['lang']=='es'):?>
                                <p class="generic-paragraph-subtitle">Gerente de Operaciones y Suministros</p>
                            <?php else:?>
                                <p class="generic-paragraph-subtitle">Operations and Supply Manager</p>
                            <?php endif;?>

                        </div>

                        <div id="aboutus-slogan" class="aboutus-content-margin">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1 class="aboutus-slogan-title">CONSTRUYENDO CAMINOS, AMPLIANDO HORIZONTES.</h1>
                            <?php else:?>
                                <h1 class="aboutus-slogan-title">BUILDING ROADS, EXPANDING HORIZONS.</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
