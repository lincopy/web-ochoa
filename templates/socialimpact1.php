            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>IMPACTO SOCIAL</h1>
                            <?php else:?>
                                <h1>SOCIAL IMPACT</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <a href="socialimpact1.php" class="generic-link-span-active">AUTOPISTA ÑU GUAZÚ <span>></span></a>
                        <?php else:?>
                            <a href="socialimpact1.php" class="generic-link-span-active">FREEWAY ÑU GUAZÚ <span>></span></a>
                        <?php endif;?>


                        <div class="wrapper-social-paragraph">
                            <?php if($_SESSION['lang']=='es'):?>

                                <h1 class="generic-title-18px">La autopista Ñu Guazú genera un beneficio económico y social para la comunidad</h1>
                                <p class="generic-title-italic">La comunidad aledaña a la autopista Ñu Guazú reconoce el beneficio económico y social que las obras viales representan para la población.</p>
                                <p class="generic-paragraph">La autopista Ñu Guazú ha significado un cambio en la vida de las familias aledañas y los comercios de la zona, pasando a ser no solo una conexión directa y rápida, sino también una oportunidad para dinamizar el comercio, revalorizar las propiedades y embellecer la comunidad.</p>
                                <p class="generic-paragraph">Eliana Gómez, pobladora del barrio Mbocayaty, relata el cambio que vivió su barrio y las facilidades que implica este nuevo acceso. “Llego en 10 minutos a Luque, antes eso era impensable”, comenta. Añade que todo cambio que beneficie a la comunidad y la población en general merece ser apoyado por los propios vecinos, porque es un beneficio para todos los habitantes.</p>
                                <p class="generic-paragraph">Para Rolando Brítez, las obras viales representan una oportunidad para mejorar la visibilidad del local donde trabaja, una conocida herrería que ya tiene 15 años de actividad y está ubicada en la zona del viaducto de la autopista Ñu Guazú. Como poblador de Luque, su traslado diario al trabajo se simplificó mucho, teniendo hoy día un acceso directo que le ayuda a trasladarse de un lugar a otro en menos de 20 minutos. Considera importante también la ventaja de los accesos viales a la hora de incrementar las ventas, con la posibilidad de duplicar la clientela diaria de la herrería en un futuro cercano.</p>
                                <p class="generic-paragraph">Tanto para Eliana como para Rolando, el progreso genera normalmente cierta resistencia en la población, que suele ser recelosa del cambio, pero que luego valora los mismos una vez que empieza a ver y disfrutar de los beneficios. “Siempre todo avance tiene su resistencia”, comenta Eliana y añade que es muy importante que las constructoras hagan partícipe a la población cuando se realizan los trabajos, agregando así el componente social que es un factor muy importante a la hora de mejorar urbanísticamente la ciudad, de manera que los cambios sean menos resistidos y mejor valorados. “Me encanta que hagan partícipe a la gente, así el progreso es valorado por todos”, finaliza.</p>

                            <?php else:?>

                                <h1 class="generic-title-18px">The Ñu Guazú highway generates an economic and social benefit for the community</h1>
                                <p class="generic-title-italic">The community bordering the Ñu Guazú highway recognizes the economic and social benefits that road works represent for the population.</p>
                                <p class="generic-paragraph">The Ñu Guazú motorway has meant a change in the lives of neighboring families and businesses in the area, becoming not only a direct and fast connection, but also an opportunity to boost commerce, revalue properties and beautify the community.</p>
                                <p class="generic-paragraph">Eliana Gómez, a resident of the Mbocayaty neighborhood, recounts the change in her neighborhood and the facilities that this new access entails. "I arrived in 10 minutes to Luque, before that was unthinkable," he says. He adds that any change that benefits the community and the population in general deserves to be supported by the neighbors themselves, because it is a benefit for all inhabitants.</p>
                                <p class="generic-paragraph">For Rolando Brítez, the roadworks represent an opportunity to improve the visibility of the place where he works, a well-known blacksmith who has already 15 years of activity and is located in the viaduct area of ​​the Ñu Guazú motorway. As a settler of Luque, his daily transfer to work was greatly simplified, having today a direct access that helps him to move from one place to another in less than 20 minutes. It also considers the advantage of road accesses to increase sales, with the possibility of doubling the daily customer base of the smithy in the near future.</p>
                                <p class="generic-paragraph">For both Eliana and Rolando, progress usually generates some resistance in the population, which is usually wary of change, but then values ​​them once they begin to see and enjoy the benefits. "Every advance has its resistance," says Eliana and adds that it is very important that the construction companies make the population participate when the work is done, adding the social component that is a very important factor when it comes to urbanizing the city , So that the changes are less resisted and better valued. "I love that they make people participate, so progress is valued by all," he says.</p>

                            <?php endif;?>

                        </div>

                        <div class="video">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/uFc4p38OoNc" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div id="wrapper-social-web">
                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>


                        </div>
                    </div>

                    <div class="col-md-4 social-wrapper-main">
                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/photo1.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/photo4.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/photo2.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work">
                            <img src="../images/social/photo3.png" class="img-responsive"/>
                        </div>

                        <div id="wrapper-social-mobile">
                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
