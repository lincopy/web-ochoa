            <?php include("header.php"); ?>
            <?php include("lang.php"); ?>
            <div id="home-header">
                <div class="home-header"></div>
                <div class="home-container">
                    <div class="home-container-logo"><h1></h1></div>
                </div>

                <div class="button-down">
                    <a id="button-down" href="#home-menu-web">
                        <img src="../images/arrow.png"/>
                    </a>
                </div>
            </div>

            <?php include("menu.php"); ?>

            <div id="home-carousel" class="container">
                <div class="row">
                    <div class="col-sm-12 mobile-padding-0px">
                        <div id="8a-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#8a-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#8a-carousel" data-slide-to="1"></li>
                                <li data-target="#8a-carousel" data-slide-to="2"></li>
                            </ol>

                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="../images/carousel/carousel-1.jpg" alt="Chania">
                                    <div class="carousel-caption">
                                        <div class="wrapper-carousel-title">
                                            <h1>OCHO A S.A.</h1>
                                        </div>

                                        <div class="wrapper-carousel-paragraph">
                                            <?php if($_SESSION['lang']=='es'):?>
                                                <h1>Obras Civiles</h1>
                                            <?php else:?>
                                                <h1>Civil works</h1>
                                            <?php endif;?>    
                                      
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <img src="../images/carousel/carousel-2.jpg" alt="Chania">
                                    <div class="carousel-caption">
                                        <div class="wrapper-carousel-title">
                                            <h1>OCHO A S.A.</h1>
                                        </div>

                                        <div class="wrapper-carousel-paragraph">
                                            <?php if($_SESSION['lang']=='es'):?>
                                               <h1>Obras Viales</h1>
                                            <?php else:?>
                                               <h1>Roadworks</h1>
                                            <?php endif;?>  
                                            
                                         
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <img src="../images/carousel/carousel-3.jpg" alt="Chania">
                                    <div class="carousel-caption">
                                        <div class="wrapper-carousel-title">
                                            <h1>OCHO A S.A.</h1>
                                        </div>

                                        <div class="wrapper-carousel-paragraph">
                                            <?php if($_SESSION['lang']=='es'):?>
                                              <h1>Obras Electromecánicas</h1>
                                            <?php else:?>
                                               <h1>Electromechanical works</h1>
                                            <?php endif;?>  
                                            
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="background-works">
                <img src="../images/3U3A0290.jpg" class="img-responsive background-works-img"/>
            </div>

            <div id="home-works" class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="wrapper-works-title">
                        <?php if($_SESSION['lang']=='es'):?>
                          <h1>NUESTRAS OBRAS</h1>
                        <?php else:?>
                          <h1>Our works</h1>
                        <?php endif;?>  
                            
                        </div>

                        <a class="wrapper-work" href="ourworks1.php">
                            <img src="../images/works/work1.jpg" class="img-responsive"/>
                            <div class="mask">
                                <h1>Carmen del Paraná</h1>
                                <h2>Depto. Itapúa</h2>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a class="wrapper-work wrapper-work-margin-top" href="ourworks3.php">
                            <img src="../images/works/work2.jpg" class="img-responsive"/>
                            <div class="mask">
                                <h1>Autopista Ñu Guazú</h1>
                                <h2>Depto. Central</h2>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-4">
                        <a class="wrapper-work wrapper-work-margin-top" href="ourworks6.php">
                            <img src="../images/works/work3.jpg" class="img-responsive"/>
                            <div class="mask">
                                <h1>Infante Rivarola</h1>
                                <h2>Depto. Boquerón</h2>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
