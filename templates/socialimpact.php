            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>IMPACTO SOCIAL</h1>
                            <?php else:?>
                                <h1>SOCIAL IMPACT</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                            <a href="socialimpact2.php" class="generic-link-span">BARRIO MBOCAYATY<span>></span></a>
                            <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                            <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                        <?php else:?>
                            <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                            <a href="socialimpact2.php" class="generic-link-span">NEIGHBORHOOD MBOCAYATY<span>></span></a>
                            <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                            <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                        <?php endif;?>

                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
