            <?php if(!isset($_SESSION)){session_start();}   ?>
            <?php include("header.php"); ?>
            <?php include("menu.php"); ?>

            <div class="background-aboutus">
                <div class="background-aboutus-layer"></div>
            </div>
            <div class="background-aboutus-line"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-aboutus-title">
                            <?php if($_SESSION['lang']=='es'):?>
                                <h1>IMPACTO SOCIAL</h1>
                            <?php else:?>
                                <h1>SOCIAL IMPACT</h1>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 aboutus-content-margin">
                        <?php if($_SESSION['lang']=='es'):?>
                            <a href="socialimpact2.php" class="generic-link-span-active">BARRIO MBOCAYATY <span>></span></a>
                        <?php else:?>
                            <a href="socialimpact2.php" class="generic-link-span-active">NEIGHBORHOOD MBOCAYATY <span>></span></a>
                        <?php endif;?>


                        <div class="wrapper-social-paragraph">
                            <?php if($_SESSION['lang']=='es'):?>

                            <h1 class="generic-title-18px">Barrio Mbocayaty: la unión de vecinos da paso al progreso</h1>
                            <p class="generic-title-italic">Un barrio caracterizado por el arduo trabajo de sus habitantes, que trabajan juntos para mejorar su comunidad, se beneficia aún más con la apertura de una nueva autopista.</p>
                            <p class="generic-paragraph">Rodeado de varias autopistas, entre ellas la nueva Autopista Ñu Guazú que une Asunción con Luque, el barrio Mbocayaty ya no es el mismo de hace décadas atrás. Eumelia Aquino, quien vive allí desde hace 40 años, comenta que antes era muy diferente. “Salía en botas de mi casa para ir a esperar colectivo, porque el barro era impresionante”, relata.</p>
                            <p class="generic-paragraph">Siendo una de las primeras habitantes de la zona, es testigo del cambio paulatino que fue viviendo Mbocayaty, desde las antiguas vías del tren y el camino po´i, hasta la nueva autopista Ñu Guazú que los conecta con mayor rapidez a Luque. “Nuestras casas se valorizan más ahora, nuestros terrenos valen mucho más, y hasta la gente quiere venir a conocer cómo quedó el barrio ahora con la autopista”, comenta entre risas.</p>
                            <p class="generic-paragraph">Ángela Caballero también recuerda el antes y después de su barrio, ya que vive allí hace más de 30 años. “Cuando llovía teníamos que tirar paja en el lodo para poder salir de nuestras casas, todo era un lodo profundo”, recuerda y añade que antes, además de no contar con los servicios básicos de agua y luz, también se sentían aislados de las ciudades porque no tenían conexión directa con el microcentro de Asunción ni Luque. Testigo y protagonista del cambio paulatino de su barrio, forma parte de la comisión de vecinos que trabajó codo a codo para empedrar sus calles, instalar una red cloacal y construir una plaza. “Ahora estamos disfrutando de todas las mejoras, pero nuestros nietos y los que vendrán disfrutarán aún más”, comenta.</p>
                            <p class="generic-paragraph">Tanto ellas como los demás vecinos consideran que toda la transformación que han vivido y que han producido ellos mismos constituye un progreso económico y social, lo que les motiva a seguir trabajando para continuar mejorando la belleza de su barrio y su propia calidad de vida.</p>

                            <?php else:?>

                                <h1 class="generic-title-18px">Mbocayaty neighborhood: the union of neighbors gives way to progress</h1>
                                <p class="generic-title-italic">A neighborhood characterized by the hard work of its inhabitants, who work together to improve their community, benefits even more with the opening of a new highway.</p>
                                <p class="generic-paragraph">Surrounded by several highways, including the new Ñu Guazú Motorway linking Asuncion and Luque, the Mbocayaty neighborhood is no longer the same as it was decades ago. Eumelia Aquino, who lives there for 40 years, says that before was very different. "I came out in boots from my house to go and wait collectively, because the mud was impressive," he says.</p>
                                <p class="generic-paragraph">Being one of the first inhabitants of the area, it witnesses the gradual change that Mbocayaty lived, from the old train tracks and the po'i road, to the new Ñu Guazú highway that connects them more quickly to Luque. "Our homes are valued more now, our land is worth much more, and even people want to come to know how the neighborhood was now with the highway," he says with laughter.</p>
                                <p class="generic-paragraph">Angela Caballero also remembers the before and after her neighborhood, since she lived there more than 30 years ago. "When it rained we had to throw straw in the mud to get out of our houses, everything was deep mud," he recalls, adding that earlier, apart from lacking basic water and light services, they also felt isolated from cities Because they had no direct connection to the microcenter of Asunción and Luque. Witness and protagonist of the gradual change of its neighborhood, it forms part of the commission of neighbors that worked side by side to cobble its streets, install a sewage network and build a square. "Now we are enjoying all the improvements, but our grandchildren and those who will come will enjoy even more," he says.</p>
                                <p class="generic-paragraph">Both they and the other neighbors consider that all the transformation that they have lived and that they have produced themselves constitutes an economic and social progress, which motivates them to continue working to continue improving the beauty of their neighborhood and their own quality of life.</p>


                            <?php endif;?>

                        </div>

                        <div class="video">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/ZCXkzAloZ1o" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div id="wrapper-social-web">

                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>


                        </div>
                    </div>

                    <div class="col-md-4 social-wrapper-main">
                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/mbocayaty/photo1.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/mbocayaty/photo4.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work margin-bottom-30px">
                            <img src="../images/social/mbocayaty/photo2.png" class="img-responsive"/>
                        </div>

                        <div class="ourworks-wrapper-work">
                            <img src="../images/social/mbocayaty/photo3.png" class="img-responsive"/>
                        </div>

                        <div id="wrapper-social-mobile">
                            <?php if($_SESSION['lang']=='es'):?>
                                <a href="socialimpact1.php" class="generic-link-span">AUTOPISTA ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARROQUIA SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POBLADORES TRASCHACO<span>></span></a>
                            <?php else:?>
                                <a href="socialimpact1.php" class="generic-link-span">FREEWAY ÑU GUAZÚ<span>></span></a>
                                <a href="socialimpact3.php" class="generic-link-span">PARISH SAN AGUSTÍN<span>></span></a>
                                <a href="socialimpact4.php" class="generic-link-span">POPULATORS TRASCHACO<span>></span></a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php include("footer.php"); ?>
