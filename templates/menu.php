            <?php
                function active($currect_page) {
                    $url_array =  explode('/', $_SERVER['REQUEST_URI']);
                    $url = end($url_array);

                    if ($currect_page == $url) {
                        echo 'active';
                    }
                }
            ?>
            <!-- <?php if(!isset($_SESSION)){session_start();}   ?> -->
            <div id="home-menu-web" class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="index.php"><div class="menu-logo"></div></a>
                        <a class="menu-iso" href="http://www.sgs.com/en/certified-clients-and-products/certified-client-directory" target="_blank"></a>

                        <div class="home-language">
                            <div class="wrapper-other">
                                 <?php if($_SESSION['lang']=='es'):?>
                                     <a class="other-label" href="http://www.ochoa.com.py/admin/login.php" target="_blank">SISTEMA DE GESTIÓN</a>
                                 <?php else:?>
                                     <a class="other-label" href="http://www.ochoa.com.py/admin/login.php" target="_blank">MANAGEMENT SYSTEM</a>
                                 <?php endif;?>    

                            </div>

                            <form class="wrapper-language">
                                <div class="radio">
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <h1 class="language-label">Idioma:</h1>
                                    <?php else:?>
                                        <h1 class="language-label">Language:</h1>
                                    <?php endif;?>

                                    <input type="radio" id="language-spanish" name="language" checked="true">
                                    <label for="language-spanish"><a href="index.php?lang=es">ESP</a></label>

                                    <input type="radio" id="language-english"  name="language">
                                    <label for="language-english"><a href="index.php?lang=en">ENG</a></label>
                                </div>
                            </form>
                        </div>

                        <div class="wrapper-menu-web">
                            <nav>
                                <ul>
                                    <?php if($_SESSION['lang']=='es'):?>
                                        <li><a class="menu-effect-underline <?php active('index.php');?>" href="index.php">Inicio</a></li>
                                        <li><a class="menu-effect-underline <?php active('aboutus.php');?>" href="aboutus.php">Quiénes somos</a></li>
                                        <li><a class="menu-effect-underline <?php active('ourworks.php'); active('ourworks1.php'); active('ourworks2.php'); active('ourworks3.php'); active('ourworks4.php');?>" href="ourworks.php">Nuestras obras</a></li>
                                        <li><a class="menu-effect-underline <?php active('socialimpact.php'); active('socialimpact1.php'); active('socialimpact2.php'); active('socialimpact3.php'); active('socialimpact4.php');?>" href="socialimpact.php">Impacto social</a></li>
                                        <li class="generic-margin-right-0px"><a class="menu-effect-underline <?php active('contact.php');?>" href="contact.php">Contacto</a></li>
                                    <?php else:?>
                                        <li><a class="menu-effect-underline <?php active('index.php');?>" href="index.php">Home</a></li>
                                        <li><a class="menu-effect-underline <?php active('aboutus.php');?>" href="aboutus.php">About Us</a></li>
                                        <li><a class="menu-effect-underline <?php active('ourworks.php'); active('ourworks1.php'); active('ourworks2.php'); active('ourworks3.php'); active('ourworks4.php');?>" href="ourworks.php">Our works</a></li>
                                        <li><a class="menu-effect-underline <?php active('socialimpact.php'); active('socialimpact1.php'); active('socialimpact2.php'); active('socialimpact3.php'); active('socialimpact4.php');?>" href="socialimpact.php">Social impact</a></li>
                                        <li class="generic-margin-right-0px"><a class="menu-effect-underline <?php active('contact.php');?>" href="contact.php">Contact</a></li>
                                    <?php endif;?>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div id="home-menu-mobile" style="display: none;">
                <input type="checkbox"/>
                <span></span> <span></span> <span></span>

                <nav class="wrapper-menu-mobile">
                    <ul>
                        <li><a class="menu-effect-underline <?php active('index.php');?>" href="index.php">Inicio</a></li>
                        <li><a class="menu-effect-underline <?php active('aboutus.php');?>" href="aboutus.php">Quiénes somos</a></li>
                        <li><a class="menu-effect-underline <?php active('ourworks.php'); active('ourworks1.php'); active('ourworks2.php'); active('ourworks3.php'); active('ourworks4.php');?>" href="ourworks.php">Nuestras obras</a></li>
                        <li><a class="menu-effect-underline <?php active('socialimpact.php'); active('socialimpact1.php'); active('socialimpact2.php'); active('socialimpact3.php'); active('socialimpact4.php');?>" href="socialimpact.php">Impacto social</a></li>
                        <li class="generic-margin-right-0px"><a class="menu-effect-underline <?php active('contact.php');?>" href="contact.php">Contacto</a></li>

                        <div class="wrapper-other">
                            <a class="other-label" href="http://www.ochoa.com.py/admin/login.php" target="_blank">SISTEMA DE GESTIÓN</a>
                        </div>

                        <form class="wrapper-language">
                            <h1 class="language-label">Idioma:</h1>
                            <div class="radio">
                                <input type="radio" id="language-spanish-mobile" name="language" checked="true">
                                <label for="language-spanish-mobile">ESP</label>

                                <input type="radio" id="language-english-mobile" name="language">
                                <label for="language-english-mobile">ENG</label>
                            </div>
                        </form>
                    </ul>
                </nav>
            </div>

            <div class="menu-logo-mobile" style="display: none;"></div>
            <a class="menu-iso-mobile" style="display: none;" href="http://www.sgs.com/en/certified-clients-and-products/certified-client-directory" target="_blank"></a>
