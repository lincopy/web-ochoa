<?php if(!isset($_SESSION)){session_start();}   ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<?php include("header.php"); ?>
<?php include("menu.php"); ?>

<div class="background-aboutus">
    <div class="background-aboutus-layer"></div>
</div>
<div class="background-aboutus-line"></div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-aboutus-title">
                <?php if($_SESSION['lang']=='es'):?>
                    <h1>CONTACTO</h1>
                <?php else:?>
                    <h1>CONTACT</h1>
                <?php endif;?>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 aboutus-content-margin">
            <?php if($_SESSION['lang']=='es'):?>
                <input id="contact-name" class="contact-input" type="text" placeholder="Nombre*" maxlength="20"/>
                <input id="contact-email" class="contact-input" type="text" placeholder="Email*" maxlength="50"/>
                <input id="contact-phone" class="contact-input" type="text" placeholder="Teléfono*" maxlength="15"/>
                <textarea id="contact-message" class="contact-textarea" rows="8" cols="80" placeholder="Mensaje"></textarea>
                <div class="g-recaptcha generic-margin-bottom-30px" data-sitekey="6Le-3A8UAAAAANhSLxeeL2_roIxTdTg_ET5tZRHi"></div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                <input id="bEnviar" type="button" class="generic-button" value="ENVIAR">
            <?php else:?>
                <input id="contact-name" class="contact-input" type="text" placeholder="Name*" maxlength="20"/>
                <input id="contact-email" class="contact-input" type="text" placeholder="Email*" maxlength="50"/>
                <input id="contact-phone" class="contact-input" type="text" placeholder="Phone*" maxlength="15"/>
                <textarea id="contact-message" class="contact-textarea" rows="8" cols="80" placeholder="Message"></textarea>
                <div class="g-recaptcha generic-margin-bottom-30px" data-sitekey="6Le-3A8UAAAAANhSLxeeL2_roIxTdTg_ET5tZRHi"></div>
                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                <input id="bEnviar" type="button" class="generic-button" value="SEND">
            <?php endif;?>

        </div>

        <div class="col-md-4">
            <div class="generic-margin-bottom-65px">
                <div class="wrapper-aboutus-content">
                    <?php if($_SESSION['lang']=='es'):?>
                        <h1>OFICINA CENTRAL</h1>
                    <?php else:?>
                        <h1>CENTRAL OFFICE</h1>
                    <?php endif;?>

                </div>

                <p class="generic-paragraph-0px">Av. Artigas 1921</p>
                <p class="generic-paragraph-0px">4to Piso</p>
                <p class="generic-paragraph-0px">+595 21 299585/8</p>
                <p class="generic-paragraph-0px">Asunción, Paraguay</p>
            </div>

            <div class="generic-margin-bottom-65px">
                <div class="wrapper-aboutus-content">
                    <?php if($_SESSION['lang']=='es'):?>
                        <h1>OPERACIONES Y SUMINISTROS</h1>
                    <?php else:?>
                        <h1>OPERATIONS AND SUPPLIES</h1>
                    <?php endif;?>

                </div>

                <p class="generic-paragraph-0px">Ruta 2 Km 18,5 esq. Santa Cruz Adorno</p>
                <p class="generic-paragraph-0px">+595 228 630125/8</p>
                <p class="generic-paragraph-0px">Capiatá, Paraguay</p>
            </div>
        </div>
    </div>
</div>
<!-- modal mensaje del form -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalForm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Info</h4>
            </div>
            <div class="modal-body">
                <p id="pForm"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="generic-button" data-dismiss="modal">Cerrar</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- -->

<?php include("footer.php"); ?>
<script type="text/javascript">
    $("#bEnviar").click(function(){
        var nombre = $("#contact-name").val();
        var email = $("#contact-email").val();
        var phone = $("#contact-phone").val();
        var message = $("#contact-message").val();
        if(nombre.trim().length<3){
            $("#pForm").text("Por favor ingrese un nombre válido");
            $("#modalForm").modal();

            return;
        };
        if(!validateEmail(email)){
            $("#pForm").text("Por favor ingrese un email válido");
            $("#modalForm").modal();
            return;
        }
        if(message.trim().length<5){
            $("#pForm").text("Por favor ingrese un mensaje válido");
            $("#modalForm").modal();
            return;
        }
        if(grecaptcha.getResponse()===""){
            $("#pForm").text("Por favor marque el captcha");
            $("#modalForm").modal();
            return;
        }
        var captchaValue = grecaptcha.getResponse();



        $.post("send_contact.php",{nombre:nombre,email:email,telefono:phone,mensaje:message,captcha:captchaValue},function(data){
            $("#pForm").text(data);
            $("#modalForm").modal();
        });

        $("#contact-name").val("");
        $("#contact-email").val("");
        $("#contact-phone").val("");
        $("#contact-message").val("");
        grecaptcha.reset();





    });
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>